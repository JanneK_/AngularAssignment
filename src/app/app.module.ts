import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { QRCodeModule } from 'angularx-qrcode';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,     
    QRCodeModule,
    FormsModule
  ],
  declarations: [
    AppComponent
  ],  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
